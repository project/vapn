<?php

/**
 * @file
 * Primary module hooks for vapn module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\NodeInterface;
use Drupal\vapn\Form\VapnSettingsForm;
use Drupal\vapn\VapnFieldDefinition;

/**
 * Implements hook_entity_bundle_field_info().
 */
function vapn_entity_bundle_field_info(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
  $fields = [];
  if ($entity_type->id() == 'node' && isset(\Drupal::config(VapnSettingsForm::CONFIG_NAME)->get('bundles')[$bundle])) {
    $fields['vapn'] = _vapn_create_field_definition();
  }
  return $fields;
}

/**
 * Implements hook_entity_field_storage_info().
 */
function vapn_entity_field_storage_info(EntityTypeInterface $entity_type) {
  $fields = [];
  if ($entity_type->id() == 'node') {
    $fields['vapn'] = _vapn_create_field_definition();
  }
  return $fields;
}

/**
 * Implements hook_entity_field_access().
 */
function vapn_entity_field_access($operation, FieldDefinitionInterface $field_definition, AccountInterface $account, ?FieldItemListInterface $items = NULL) {
  if ($field_definition->getName() == 'vapn') {
    $access_result = AccessResult::forbidden();
    foreach (['administer vapn', 'use vapn'] as $permission) {
      if ($account->hasPermission($permission)) {
        $access_result = AccessResult::allowed();
        break;
      }
    }
    return $access_result->addCacheContexts(['user.permissions']);
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_ENTITY_TYPE_access() for node.
 */
function vapn_node_access(NodeInterface $entity, $operation, AccountInterface $account) {
  if ($operation == 'view' && $entity->hasField('vapn')) {
    $access_result = AccessResult::allowedIfHasPermission($account, 'bypass vapn');
    if ($access_result->isAllowed()) {
      return $access_result;
    }

    $allowed_roles = [];
    foreach ($entity->get('vapn') as $item) {
      $rid = $item->target_id;
      $allowed_roles[$rid] = $rid;
    }
    $access_result = array_intersect($account->getRoles(), $allowed_roles)
      ? AccessResult::allowed()
      : AccessResult::forbidden();
    return $access_result->addCacheableDependency($entity)
      ->addCacheContexts(['user:roles']);
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_field_widget_complete_form_alter().
 */
function vapn_field_widget_complete_form_alter(&$field_widget_complete_form, FormStateInterface $form_state, $context) {
  // Move the VAPN field to the advanced section.
  if (isset($context['form']['advanced']) && $context['items']?->getFieldDefinition()->getName() == 'vapn') {
    $field_widget_complete_form = [
      '#type' => 'details',
      '#title' => t('View access per node'),
      '#group' => 'advanced',
      '#open' => FALSE,
      'content' => $field_widget_complete_form,
    ];
  }
}

/**
 * Creates the VAPN field definition.
 */
function _vapn_create_field_definition() {
  return VapnFieldDefinition::create('entity_reference')
    ->setLabel(t('VAPN'))
    ->setDescription(t('List of roles for access to the node.'))
    ->setSetting('target_type', 'user_role')
    ->setDisplayConfigurable('form', TRUE)
    ->setDisplayOptions('form', [
      'type' => 'options_buttons',
      'weight' => 100,
    ])
    ->setDisplayConfigurable('view', TRUE)
    ->setDisplayOptions('view', [
      'region' => 'hidden',
    ])
    ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED);
}

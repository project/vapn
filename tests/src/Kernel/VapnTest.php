<?php

namespace Drupal\Tests\vapn\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\node\Entity\Node;
use Drupal\vapn\Form\VapnSettingsForm;

/**
 * Tests the vapn module.
 */
class VapnTest extends KernelTestBase {

  use ContentTypeCreationTrait;
  use NodeCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'text',
    'filter',
    'node',
    'vapn',
  ];

  /**
   * The target node.
   */
  protected Node $nodeWithVapn;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter', 'node']);
    $this->installSchema('node', ['node_access']);
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');

    // Create user 1 so the test user doesn't bypass access control.
    $this->createUser();

    // Configure the vapn module.
    $node_type = $this->createContentType()->id();
    $this->config(VapnSettingsForm::CONFIG_NAME)
      ->set('bundles', [$node_type => TRUE])
      ->save();
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();

    // Create a node with a vapn field.
    $this->nodeWithVapn = $this->createNode(['type' => $node_type]);
  }

  /**
   * Tests the vapn field existence.
   */
  public function testFieldExistence() {
    $this->assertTrue($this->nodeWithVapn->hasField('vapn'));
    $this->assertFalse($this->createNode()->hasField('vapn'));
  }

  /**
   * Tests the vapn field access.
   */
  public function testFieldAccess() {
    $vapn_field = $this->nodeWithVapn->get('vapn');
    $this->assertFalse($vapn_field->access('edit', $this->createUser()));
    $this->assertTrue($vapn_field->access('edit', $this->createUser(['administer vapn'])));
    $this->assertTrue($vapn_field->access('edit', $this->createUser(['use vapn'])));
  }

  /**
   * Tests the node access.
   */
  public function testNodeAccess() {
    $role = $this->createRole(['access content']);
    $this->nodeWithVapn->set('vapn', ['target_id' => $role])->save();
    $this->assertFalse($this->nodeWithVapn->access('view', $this->createUser(values: ['roles' => []])));
    $this->assertTrue($this->nodeWithVapn->access('view', $this->createUser(values: ['roles' => [['target_id' => $role]]])));
  }

}

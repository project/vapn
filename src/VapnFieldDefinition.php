<?php

namespace Drupal\vapn;

use Drupal\Core\Field\BaseFieldDefinition;

/**
 * A class for defining bundle entity fields.
 *
 * @todo revisit after https://www.drupal.org/project/drupal/issues/2346347 is resolved.
 */
final class VapnFieldDefinition extends BaseFieldDefinition {

  /**
   * {@inheritdoc}
   */
  public function isBaseField() {
    return FALSE;
  }

}

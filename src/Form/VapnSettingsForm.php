<?php

namespace Drupal\vapn\Form;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\Checkboxes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure vapn settings for this site.
 */
class VapnSettingsForm extends ConfigFormBase {

  /**
   * The module config name.
   */
  const CONFIG_NAME = 'vapn.settings';

  /**
   * The entity bundle info.
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * The entity field manager.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    $instance->entityFieldManager = $container->get('entity_field.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'vapn_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $options = [];
    foreach ($this->entityTypeBundleInfo->getBundleInfo('node') as $bundle => $info) {
      $options[$bundle] = $info['label'];
    }
    $bundles = $this->config(static::CONFIG_NAME)->get('bundles');
    $form['bundles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Bundles'),
      '#options' => $options,
      '#default_value' => $bundles ? array_keys($bundles) : [],
      '#description' => $this->t('Select content types that should have the view permission on each node.'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $bundles = [];
    foreach (Checkboxes::getCheckedCheckboxes($form_state->getValue('bundles')) as $bundle) {
      $bundles[$bundle] = TRUE;
    }
    $this->config(static::CONFIG_NAME)
      ->set('bundles', $bundles)
      ->save();
    $this->entityFieldManager->clearCachedFieldDefinitions();

    parent::submitForm($form, $form_state);
  }

}
